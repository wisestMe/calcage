﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Services;
using System.Text;
using System.Threading.Tasks;

namespace CalcAge.services
{
    public class GetDOB
    {
        public static int GetUserDOB()
        {
            var ukCulture = new System.Globalization.CultureInfo("en-UK");
            Console.WriteLine("Enter your D.O.B using format:" + ukCulture.DateTimeFormat.ShortDatePattern);
            string userDOBString = Console.ReadLine();

            DateTime userDOB;
            if (DateTime.TryParse(userDOBString, ukCulture.DateTimeFormat, System.Globalization.DateTimeStyles.None, out userDOB)) 
            Console.WriteLine();
            else
                Console.WriteLine("Invalid date specified");

            int currentYear = DateTime.Now.Year;
            int birthYear = userDOB.Year;

            int userAge = currentYear - birthYear;
            return userAge;
        }
    }
}
