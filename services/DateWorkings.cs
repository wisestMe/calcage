﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcAge.services
{
    class DateWorkings
    {
        public static void DateResults()
        {
            DateTime dt = new DateTime();
            Console.WriteLine(dt);

            DateTime dt2 = DateTime.Now;
            Console.WriteLine(dt2.ToString("dd'/'MM yyyy"));

            DateTime dt3 = new DateTime(2042, 12, 24);
            Console.WriteLine(dt3.ToLongDateString());

            DateTime dt4 = new DateTime(2042, 12, 24, 18, 42, 0);
            Console.WriteLine(dt4.Date.ToShortDateString());

            var ukCulture = new System.Globalization.CultureInfo("en-UK");
            Console.WriteLine("Please specify a date. follow format:" + ukCulture.DateTimeFormat.ShortDatePattern);
            string dateString = Console.ReadLine();
            DateTime userDate = DateTime.Parse(dateString, ukCulture.DateTimeFormat);
            Console.WriteLine("Date entered (long date format):" + userDate.ToLongDateString());
        }
    }
}
